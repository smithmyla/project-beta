import React, { useState, } from 'react';

function ServiceHistoryList(props) {
    const [searchVin, setSearchVin] = useState('')
    const filterSearch = props.appointments.filter((appointment) =>
        appointment.vin.toLowerCase().includes(searchVin.toLowerCase())
    );

    return (
        <div className="text-center">
            <h1 className="display-7 fw-bold">Service History</h1>
            <div className="mb-3">
                <input
                    type="text"
                    style={{width: "20%"}}
                    className="form-control"
                    placeholder="Search by VIN"
                    value={searchVin}
                    onChange={(e) => setSearchVin(e.target.value)}
                />
            </div>
            <table className="table table-hover table-bordered border-dark">
                <thead>
                    <tr className="table-success table-bordered border-dark">
                        <th>Vin</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filterSearch.map((appointment) => {
                        const date = new Date(appointment.date_time).toLocaleDateString({hour12: true});
                        const time = new Date(appointment.date_time).toLocaleTimeString([], {hour: "2-digit", minute: "2-digit"});
                        return (
                            <tr key={appointment.id}>
                                <td className="fw-bold">{appointment.vin}</td>
                                <td>{appointment.vin_in_system ? "Yes" : "No"}</td>
                                <td>{appointment.customer}</td>
                                <td>{date}</td>
                                <td>{time}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistoryList;

