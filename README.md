# CarCar

Team:

* Myla - Service
* Ricky - Sales

## Design

## Service microservice

There will be a Technician, AutomobileVO, and Appointment model. The AutomobileVO is a model from the Inventory microservice, in which Service needs that information so I'll be polling Inventory to Service so that Inventory can send Automobile's data back to Service.

## Sales microservice

Going to create three models. Salesperson model containing first and last name, employee id. A customer model containing first and last name, address, and phone number. A sale model containing automobile, salesperson, customer, and price field. As well as a AutomobileVO model containing the vin.
