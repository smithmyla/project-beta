import React, { useState, useEffect } from 'react';

function SalesForm() {

    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalesPersons] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobile, setAutomobile] = useState("");
    const [salesperson, setSalesPerson] = useState("");
    const [customer, setCustomer] = useState("");
    const [price, setPrice] = useState("");


    const fetchData = async () => {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const salesUrl = 'http://localhost:8090/api/salespeople/';
        const customerUrl = 'http://localhost:8090/api/customers/';
        const autoResponse = await fetch(autoUrl);
        const salesResponse = await fetch(salesUrl);
        const customerResponse = await fetch(customerUrl);

        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
            setAutomobiles(autoData.autos);
        }
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSalesPersons(salesData.salespeople);
        }
        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            setCustomers(customerData.customers);
        }
    }


    useEffect(() => {
        fetchData();
    },[]);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile;
        data.salesperson = parseInt(salesperson);
        data.customer = parseInt(customer);
        data.price = parseInt(price);
    const url = 'http://localhost:8090/api/sales/';

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        window.location.href = "http://localhost:3000/sales"
    }
}

    const handleChangeAutomobile = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleSalesPersonChange = (event) => {
        setSalesPerson(event.target.value);
    }
    const handleCustomersChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a new sale</h1>
                <form onSubmit={handleSubmit} id="create-sales-form">
                    <div className="mb-3">
                        <select onChange={handleChangeAutomobile} name="automobile" id="automobile" value={automobile.vin} className="form-select">
                            <option value="">Choose an automobile ...</option>
                            {automobiles?.map(automobile => {
                                return (
                                    <option key={automobile.id} value={automobile.vin}>{automobile.vin} {automobile.year} {automobile.color} {automobile.model.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleSalesPersonChange} name="salesperson" id="salesperson" value={salesperson} className="form-select">
                            <option value="">Choose a salesperson...</option>
                            {salespeople?.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleCustomersChange} name="customer" id="customer" className="form-select">
                            <option value="">Choose a customer...</option>
                            {customers?.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePriceChange}placeholder="price" required type="text" name="price" value={price} id="price" className="form-control"/>
                        <label htmlFor="phone_number">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
    }

export default SalesForm;
