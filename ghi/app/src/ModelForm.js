import React, { useEffect, useState } from 'react';

function ModelForm() {
  const [manufacturers, setManufacturers] = useState([])

  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {

      setFormData({
        name: '',
        picture_url: '',
        manufacturer: '',
      });
      window.location.href = 'http://localhost:3000/models/';
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,

      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Model</h1>
          <form onSubmit={handleSubmit} id="create-model-form">

            <div className="form-floating mb-3">

              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" value={formData.name} id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Picture" required type="url" name="picture_url" value={formData.picture_url} id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Paste Link URL Here...</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} required name="manufacturer_id" value={formData.manufacturer_id} id="manufacturer" className="form-select">
                <option value="">Assign a Manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-lg btn-success">Create Vehicle Model</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ModelForm;
