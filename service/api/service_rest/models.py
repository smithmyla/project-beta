from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_list_technicians", kwargs={"id": self.id})


class Appointment(models.Model):

    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=10, default="created")
    vin = models.CharField(max_length=17)
    vin_in_system = models.BooleanField(default=False)

    customer = models.CharField(max_length=200)

    technician = models.ForeignKey(
        Technician,
        related_name ="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.customer
