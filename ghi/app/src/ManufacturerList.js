import React, { useState, } from 'react';

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState(props.manufacturers);

    const deleteManufacturer = async (id) => {
      const url = `http://localhost:8100/api/manufacturers/${id}/`;
      try {
        const response = await fetch(url, {
            method: 'DELETE'
        });
        if (response.ok) {
            setManufacturers(manufacturers.filter((manufacturer) => manufacturer.id !== id));
            window.location.reload();
        }
    } catch (error) {
        console.error('Could not remove manufacturer:', error);
        };
    }

        return (
            <div className="text-center">
                <h1 className="display-6 fw-bold">Manufacturers</h1>
                <table className="table table-hover table-bordered border-dark">
                    <thead>
                        <tr className="table-success table-bordered border-dark">
                            <th>Name</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.id}>
                                    <td>{manufacturer.name}</td>
                                    <td>
                                        <button className="btn btn-outline-danger fw-bold" onClick={() => deleteManufacturer(manufacturer.id)}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

export default ManufacturerList;
