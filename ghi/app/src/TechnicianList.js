import React, { useState, } from 'react';

function TechnicianList(props) {
    const [technicians, setTechnicians] = useState(props.technicians);

    const deleteTechnician = async (id) => {
      const url = `http://localhost:8080/api/technicians/${id}/`;
      try {
        const response = await fetch(url, {
            method: 'DELETE'
        });
        if (response.ok) {
            setTechnicians(technicians.filter((technician) => technician.id !== id));
            window.location.reload();
        }
    } catch (error) {
        console.error('Could not cancel appointment:', error);
        };
    }

        return (
            <div className="text-center">
                <h1 className="display-6 fw-bold">Technicians</h1>
                <table className="table table-hover table-bordered border-dark">
                    <thead>
                        <tr className="table-success table-bordered border-dark">
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Employee ID</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.technicians.map(technician => {
                            return (
                                <tr key={technician.id}>
                                    <td>{technician.first_name}</td>
                                    <td>{technician.last_name}</td>
                                    <td>{technician.employee_id}</td>
                                    <td>
                                        <button className="btn btn-outline-danger fw-bold" onClick={() => deleteTechnician(technician.id)}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

export default TechnicianList;
