import React, {useEffect, useState,} from 'react';

function SalesPeopleList() {
    const [salespeople, setSalesPeople] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    }

    const deleteSalesPerson = async (id) => {
        const url = `http://localhost:8090/api/salespeople/${id}/`;
        const response = await fetch(url, {
            method: 'DELETE'
        });
        if (response.ok) {
            setSalesPeople(salespeople.filter((salesperson) => salesperson.id !== id));
        }
    }

    useEffect(() => {
        fetchData();
    },[]);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee Id</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                            <td>{salesperson.employee_id}</td>
                            <td>
                                <button onClick={() => deleteSalesPerson(salesperson.id)}>Delete</button>
                            </td>
                        </tr>
                    );

                })}
            </tbody>
        </table>
    );
}
export default SalesPeopleList;
